<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Package as Package;
use App\Http\Services\GenerateModel as GenerateModel;
use App\Http\Services\PackageValidation as PackageValidation;
use App\Models\CustomerAtrribute as CustomerAtrribute;
use Illuminate\Support\MessageBag;
use Illuminate\Support\Facades\Validator;

class PackageController extends Controller
{
    //
    public function index()
    {
        $data = Package::all(); 
        return $data;
    }

    public function findById($id)
    {
        $data = Package::where('transaction_id',$id)->first(); 
        if($data == null){
            return response($id,204);        
        }
        return $data;
    }

    public function save(Request $request)
    {
        $packageValidation = new PackageValidation();
        $rules=$packageValidation->getValidationOnUpdate();

        $validator=Validator::make($request->all(),$rules);
        if($validator->fails())
        {
            $messages=$validator->messages();
            $errors=$messages->all();
            return response($errors,500); 
        }
        $GenerateModel = new GenerateModel();
        $data = Package::create([
            'transaction_id' => $request->transaction_id,
            'customer_name' => $request->customer_name,
            'customer_code' => $request->customer_code,
            'transaction_amount' => $request->transaction_amount,
            'transaction_discount' => $request->transaction_discount,
            'transaction_additional_field' => $request->transaction_additional_field,
            'transaction_payment_type' => $request->transaction_payment_type,
            'transaction_state' => $request->transaction_state,
            'transaction_code' => $request->transaction_code,
            'transaction_order' => $request->transaction_order,
            'location_id' => $request->location_id,
            'organization_id' => $request->organization_id,
            'created_at' => $request->created_at,
            'updated_at' => $request->updated_at,
            'transaction_payment_type_name' => $request->transaction_payment_type_name,
            'transaction_cash_amount' => $request->transaction_cash_amount,
            'transaction_cash_change' => $request->transaction_cash_change,
            'customer_attribute' => $GenerateModel->createModelCustomerAtrribute($request),
            'connote' => $GenerateModel->createModelConnote($request),
            'connote_id' => $request->connote_id,
            'origin_data' => $GenerateModel->createModelOrigin($request),
            'destination_data' => $GenerateModel->createModelDestination($request),
            'koli_data' => $request->koli_data,
            'custom_field' => $GenerateModel->createModelCustomField($request),
            'currentLocation' => $GenerateModel->createModelCurrentLocation($request)
        ]); 
        $data->save();
        return $data;
    }

    public function update($id, Request $request)
    {
        $data = Package::where('transaction_id',$id)->first(); 
        if($data == null){
            return response($id,204);        
        }

        $packageValidation = new PackageValidation();
        $rules=$packageValidation->getValidationOnUpdate();
        $validator=Validator::make($request->all(),$rules);
        if($validator->fails())
        {
            $messages=$validator->messages();
            $errors=$messages->all();
            return response($errors,500); 
        }
        
        $GenerateModel = new GenerateModel();
        $data->customer_name = $request->customer_name;
        $data->customer_code = $request->customer_code;
        $data->transaction_amount = $request->transaction_amount;
        $data->transaction_discount = $request->transaction_discount;
        $data->transaction_additional_field = $request->transaction_additional_field;
        $data->transaction_payment_type = $request->transaction_payment_type;
        $data->transaction_state = $request->transaction_state;
        $data->transaction_code = $request->transaction_code;
        $data->transaction_order = $request->transaction_order;
        $data->location_id = $request->location_id;
        $data->updated_at = $request->updated_at;
        $data->transaction_payment_type_name = $request->transaction_payment_type_name;
        $data->transaction_cash_amount = $request->transaction_cash_amount;
        $data->transaction_cash_change = $request->transaction_cash_change;
        $data->customer_attribute = $GenerateModel->createModelCustomerAtrribute($request);
        $data->connote = $GenerateModel->createModelConnote($request);
        $data->connote_id = $request->connote_id;
        $data->origin_data = $GenerateModel->createModelOrigin($request);
        $data->destination_data = $GenerateModel->createModelDestination($request);
        $data->koli_data = $request->koli_data;
        $data->custom_field = $GenerateModel->createModelCustomField($request);
        $data->currentLocation = $GenerateModel->createModelCurrentLocation($request);

        $data->save();
        return $data;
    }


    public function updatePatch($id, Request $request)
    {
        $data = Package::where('transaction_id',$id)->first(); 
        if($data == null){
            return response($id,204);        
        }

        $packageValidation = new PackageValidation();
        $rules=$packageValidation->getValidationOnPatch();
        $validator=Validator::make($request->all(),$rules);
        if($validator->fails())
        {
            $messages=$validator->messages();
            $errors=$messages->all();
            return response($errors,500); 
        }

        $GenerateModel = new GenerateModel();
        if($request->customer_name) $data->customer_name = $request->customer_name;
        if($request->customer_code) $data->customer_code = $request->customer_code;
        if($request->transaction_amount) $data->transaction_amount = $request->transaction_amount;
        if($request->transaction_discount) $data->transaction_discount = $request->transaction_discount;
        if($request->transaction_additional_field) $data->transaction_additional_field = $request->transaction_additional_field;
        if($request->transaction_payment_type) $data->transaction_payment_type = $request->transaction_payment_type;
        if($request->transaction_state) $data->transaction_state = $request->transaction_state;
        if($request->transaction_code) $data->transaction_code = $request->transaction_code;
        if($request->transaction_order) $data->transaction_order = $request->transaction_order;
        if($request->location_id) $data->location_id = $request->location_id;
        if($request->updated_at) $data->updated_at = $request->updated_at;
        if($request->transaction_payment_type_name) $data->transaction_payment_type_name = $request->transaction_payment_type_name;
        if($request->transaction_cash_amount) $data->transaction_cash_amount = $request->transaction_cash_amount;
        if($request->transaction_cash_change) $data->transaction_cash_change = $request->transaction_cash_change;
        if($request->customer_attribute) $data->customer_attribute = $GenerateModel->createModelCustomerAtrribute($request);
        if($request->connote) $data->connote = $GenerateModel->createModelConnote($request);
        if($request->connote_id) $data->connote_id = $request->connote_id;
        if($request->origin_data) $data->origin_data = $GenerateModel->createModelOrigin($request);
        if($request->destination_data) $data->destination_data = $GenerateModel->createModelDestination($request);
        if($request->koli_data) $data->koli_data = $request->koli_data;
        if($request->custom_field) $data->custom_field = $GenerateModel->createModelCustomField($request);
        if($request->currentLocation) $data->currentLocation = $GenerateModel->createModelCurrentLocation($request);
        $data->save();

        return $data;
    }


    public function delete($id)
    {
        $data = Package::where('transaction_id',$id)->first(); 
        if($data == null){
            return response($id,204);        
        }
        $data->delete();
        return $id;
    }
}
