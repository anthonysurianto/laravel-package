<?php

namespace App\Http\Services;

use Illuminate\Http\Request;
use App\Models\CustomerAtrribute as CustomerAtrribute;

class PackageValidation
{

    public function getValidationOnUpdate(){
        return array(
            'transaction_id' => 'required|max:255',
            'customer_name' => 'required|max:255',
            'customer_code' => 'required|numeric',
            'transaction_amount' => 'required|numeric',
            'transaction_discount' => 'required|numeric',
            'transaction_additional_field' => 'nullable',
            'transaction_payment_type' => 'required|numeric',
            'transaction_state' => 'required|max:25',
            'transaction_code' => 'required|max:255',
            'transaction_order' => 'required|numeric',
            'location_id' => 'required|max:255',
            'organization_id' => 'required|numeric',
            'created_at' => 'required',
            'updated_at' => 'required',
            'transaction_payment_type_name' => 'required|max:255',
            'transaction_cash_amount' => 'required|numeric',
            'transaction_cash_change' => 'required|numeric',

            'customer_attribute' => 'required',
            'customer_attribute.Nama_Sales' => 'required|max:255',
            'customer_attribute.TOP' => 'required|max:255',
            'customer_attribute.Jenis_Pelanggan' => 'required|max:255',

            'connote' => 'required',
            'connote.connote_id' => 'required|max:255',
            'connote.connote_number' => 'required|numeric',
            'connote.connote_service' => 'required|max:255',
            'connote.connote_service_price' => 'required|numeric',
            'connote.connote_amount' => 'required|numeric',
            'connote.connote_code' => 'required|max:255',
            'connote.connote_booking_code' => 'max:255|nullable',
            'connote.connote_order' => 'required|numeric',
            'connote.connote_state' => 'required|max:255',
            'connote.connote_state_id' => 'required|numeric',
            'connote.zone_code_from' => 'required|max:255',
            'connote.zone_code_to' => 'required|max:255',
            'connote.surcharge_amount' => 'nullable|numeric',
            'connote.transaction_id' => 'required|max:255',
            'connote.actual_weight' => 'required|numeric',
            'connote.volume_weight' => 'required|numeric',
            'connote.chargeable_weight' => 'required|numeric',
            'connote.created_at' => 'required',
            'connote.updated_at' => 'required',
            'connote.location_id' => 'required|max:255',
            'connote.connote_total_package' => 'required|numeric',
            'connote.connote_surcharge_amount' => 'required|numeric',
            'connote.connote_sla_day' => 'required|numeric',
            'connote.location_name' => 'required|max:255',
            'connote.location_type' => 'required|max:255',
            'connote.source_tariff_db' => 'required|max:255',
            'connote.id_source_tariff' => 'required|numeric',
            'connote.pod' => 'nullable',
            'connote.history' => 'nullable',

            'connote_id' => 'required|max:255',

            'origin_data' => 'required',
            'origin_data.customer_name' => 'required|max:255',
            'origin_data.customer_address' => 'required|max:255',
            'origin_data.customer_email' => 'nullable|max:255',
            'origin_data.customer_phone' => 'required|max:255',
            'origin_data.customer_address_detail' => 'nullable',
            'origin_data.customer_zip_code' => 'required|numeric',
            'origin_data.zone_code' => 'required|max:255',
            'origin_data.organization_id' => 'required|numeric',
            'origin_data.location_id' => 'required|max:255',

            'destination_data' => 'required',
            'destination_data.customer_name' => 'required|max:255',
            'destination_data.customer_address' => 'required|max:255',
            'destination_data.customer_email' => 'nullable|max:255',
            'destination_data.customer_phone' => 'required|max:255',
            'destination_data.customer_address_detail' => 'nullable',
            'destination_data.customer_zip_code' => 'required|numeric',
            'destination_data.zone_code' => 'required|max:255',
            'destination_data.organization_id' => 'required|numeric',
            'destination_data.location_id' => 'required|max:255',

            'koli_data' => 'required',

            'custom_field' => 'required',
            'custom_field.catatan_tambahan' => 'nullable|max:255',

            'currentLocation' => 'required',
            'currentLocation.name' => 'required|max:255',
            'currentLocation.code' => 'required|max:255',
            'currentLocation.type' => 'required|max:255'
        );
    }

    public function getValidationOnPatch(){
        return array(
            'transaction_id' => 'max:255',
            'customer_name' => 'max:255',
            'customer_code' => 'numeric',
            'transaction_amount' => 'numeric',
            'transaction_discount' => 'numeric',
            'transaction_additional_field' => 'nullable',
            'transaction_payment_type' => 'numeric',
            'transaction_state' => 'max:25',
            'transaction_code' => 'max:255',
            'transaction_order' => 'numeric',
            'location_id' => 'max:255',
            'organization_id' => 'numeric',
            'created_at' => '',
            'updated_at' => '',
            'transaction_payment_type_name' => 'max:255',
            'transaction_cash_amount' => 'numeric',
            'transaction_cash_change' => 'numeric',

            // 'customer_attribute' => 'required',
            'customer_attribute.Nama_Sales' => 'max:255',
            'customer_attribute.TOP' => 'max:255',
            'customer_attribute.Jenis_Pelanggan' => 'max:255',

            // 'connote' => 'required',
            'connote.connote_id' => 'max:255',
            'connote.connote_number' => 'numeric',
            'connote.connote_service' => 'max:255',
            'connote.connote_service_price' => 'numeric',
            'connote.connote_amount' => 'numeric',
            'connote.connote_code' => 'max:255',
            'connote.connote_booking_code' => 'max:255|nullable',
            'connote.connote_order' => 'numeric',
            'connote.connote_state' => 'max:255',
            'connote.connote_state_id' => 'numeric',
            'connote.zone_code_from' => 'max:255',
            'connote.zone_code_to' => 'max:255',
            'connote.surcharge_amount' => 'nullable|numeric',
            'connote.transaction_id' => 'max:255',
            'connote.actual_weight' => 'numeric',
            'connote.volume_weight' => 'numeric',
            'connote.chargeable_weight' => 'numeric',
            'connote.location_id' => 'max:255',
            'connote.connote_total_package' => 'numeric',
            'connote.connote_surcharge_amount' => 'numeric',
            'connote.connote_sla_day' => 'numeric',
            'connote.location_name' => 'max:255',
            'connote.location_type' => 'max:255',
            'connote.source_tariff_db' => 'max:255',
            'connote.id_source_tariff' => 'numeric',
            'connote.pod' => 'nullable',
            'connote.history' => 'nullable',

            'connote_id' => 'max:255',

            // 'origin_data' => 'required',
            'origin_data.customer_name' => 'max:255',
            'origin_data.customer_address' => 'max:255',
            'origin_data.customer_email' => 'nullable|max:255',
            'origin_data.customer_phone' => 'max:255',
            'origin_data.customer_address_detail' => 'nullable',
            'origin_data.customer_zip_code' => 'numeric',
            'origin_data.zone_code' => 'max:255',
            'origin_data.organization_id' => 'numeric',
            'origin_data.location_id' => 'max:255',

            // 'destination_data' => 'required',
            'destination_data.customer_name' => 'max:255',
            'destination_data.customer_address' => 'max:255',
            'destination_data.customer_email' => 'nullable|max:255',
            'destination_data.customer_phone' => 'max:255',
            'destination_data.customer_address_detail' => 'nullable',
            'destination_data.customer_zip_code' => 'numeric',
            'destination_data.zone_code' => 'max:255',
            'destination_data.organization_id' => 'numeric',
            'destination_data.location_id' => 'max:255',

            // 'koli_data' => 'required',

            // 'custom_field' => 'required',
            'custom_field.catatan_tambahan' => 'nullable|max:255',

            // 'currentLocation' => 'required',
            'currentLocation.name' => 'max:255',
            'currentLocation.code' => 'max:255',
            'currentLocation.type' => 'max:255'
        );
    }

}