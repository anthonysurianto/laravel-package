<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/package', 'App\Http\Controllers\PackageController@index');
Route::get('/package/{id}', 'App\Http\Controllers\PackageController@findById');
Route::post('/package', 'App\Http\Controllers\PackageController@save');
Route::put('/package/{id}', 'App\Http\Controllers\PackageController@update');
Route::patch('/package/{id}', 'App\Http\Controllers\PackageController@updatePatch');
Route::delete('/package/{id}', 'App\Http\Controllers\PackageController@delete');