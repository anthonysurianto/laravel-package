<?php

namespace App\Models;


class Connote
{
    public $connote_id;
    public $connote_number;
    public $connote_service;
    public $connote_service_price;
    public $connote_amount;
    public $connote_code;
    public $connote_booking_code;
    public $connote_order;
    public $connote_state;
    public $connote_state_id;
    public $zone_code_from;
    public $zone_code_to;
    public $surcharge_amount;
    public $actual_weight;
    public $volume_weight;
    public $chargeable_weight;
    public $created_at;
    public $updated_at;
    public $location_id;
    public $connote_total_package;
    public $connote_surcharge_amount;
    public $connote_sla_day;
    public $location_name;
    public $location_type;
    public $source_tariff_db;
    public $id_source_tariff;
    public $pod;
    public $history;
}