<?php

namespace App\Http\Services;

use Illuminate\Http\Request;
use App\Models\CustomerAtrribute as CustomerAtrribute;
use App\Models\Connote as Connote;
use App\Models\OriginDestination as OriginDestination;
use App\Models\CustomField as CustomField;
use App\Models\CurrentLocation as CurrentLocation;

class GenerateModel
{
    public function createModelCustomerAtrribute(Request $request){
        $customer_attribute = new CustomerAtrribute();
        $customer_attribute->Nama_Sales = $request->customer_attribute['Nama_Sales'];
        $customer_attribute->TOP = $request->customer_attribute['TOP'];
        $customer_attribute->Jenis_Pelanggan = $request->customer_attribute['Jenis_Pelanggan'];
        return $customer_attribute;
    }

    public function createModelConnote(Request $request){
        $connote = new Connote();
        $connote->connote_id = $request->connote['connote_id'];
        $connote->connote_number = $request->connote['connote_number'];
        $connote->connote_service = $request->connote['connote_service'];
        $connote->connote_service_price = $request->connote['connote_service_price'];
        $connote->connote_amount = $request->connote['connote_amount'];
        $connote->connote_code = $request->connote['connote_code'];
        $connote->connote_booking_code = $request->connote['connote_booking_code'];
        $connote->connote_order = $request->connote['connote_order'];
        $connote->connote_state = $request->connote['connote_state'];
        $connote->connote_state_id = $request->connote['connote_state_id'];
        $connote->zone_code_from = $request->connote['zone_code_from'];
        $connote->zone_code_to = $request->connote['zone_code_to'];
        $connote->surcharge_amount = $request->connote['surcharge_amount'];
        $connote->actual_weight = $request->connote['actual_weight'];
        $connote->volume_weight = $request->connote['volume_weight'];
        $connote->chargeable_weight = $request->connote['chargeable_weight'];
        $connote->created_at = $request->connote['created_at'];
        $connote->updated_at = $request->connote['updated_at'];
        $connote->location_id = $request->connote['location_id'];
        $connote->connote_total_package = $request->connote['connote_total_package'];
        $connote->connote_surcharge_amount = $request->connote['connote_surcharge_amount'];
        $connote->connote_sla_day = $request->connote['connote_sla_day'];
        $connote->location_name = $request->connote['location_name'];
        $connote->location_type = $request->connote['location_type'];
        $connote->source_tariff_db = $request->connote['source_tariff_db'];
        $connote->id_source_tariff = $request->connote['id_source_tariff'];
        $connote->pod = $request->connote['pod'];
        $connote->history = $request->connote['history'];
        return $connote;
    }

    public function createModelOrigin(Request $request){
        $origin_data = new OriginDestination();
        $origin_data->customer_name = $request->origin_data['customer_name'];
        $origin_data->customer_address = $request->origin_data['customer_address'];
        $origin_data->customer_email = $request->origin_data['customer_email'];
        $origin_data->customer_phone = $request->origin_data['customer_phone'];
        $origin_data->customer_address_detail = $request->origin_data['customer_address_detail'];
        $origin_data->customer_zip_code = $request->origin_data['customer_zip_code'];
        $origin_data->zone_code = $request->origin_data['zone_code'];
        $origin_data->organization_id = $request->origin_data['organization_id'];
        $origin_data->location_id = $request->origin_data['location_id'];
        return $origin_data;
    }

    public function createModelDestination(Request $request){
        $destination_data = new OriginDestination();
        $destination_data->customer_name = $request->destination_data['customer_name'];
        $destination_data->customer_address = $request->destination_data['customer_address'];
        $destination_data->customer_email = $request->destination_data['customer_email'];
        $destination_data->customer_phone = $request->destination_data['customer_phone'];
        $destination_data->customer_address_detail = $request->destination_data['customer_address_detail'];
        $destination_data->customer_zip_code = $request->destination_data['customer_zip_code'];
        $destination_data->zone_code = $request->destination_data['zone_code'];
        $destination_data->organization_id = $request->destination_data['organization_id'];
        $destination_data->location_id = $request->destination_data['location_id'];
        return $destination_data;
    }

    public function createModelCustomField(Request $request){
        $custom_field = new CustomField();
        $custom_field->catatan_tambahan = $request->custom_field['catatan_tambahan'];
        return $custom_field;
    }

    public function createModelCurrentLocation(Request $request){
        $currentLocation = new CurrentLocation();
        $currentLocation->name = $request->currentLocation['name'];
        $currentLocation->code = $request->currentLocation['code'];
        $currentLocation->type = $request->currentLocation['type'];
        return $currentLocation;
    }
}