<?php

namespace App\Models;


class OriginDestination
{
    public $customer_name;
    public $customer_address;
    public $customer_email;
    public $customer_phone;
    public $customer_address_detail;
    public $customer_zip_code;
    public $zone_code;
    public $organization_id;
    public $location_id;
}